���?�?���?����?��������??���?��Ƶ??�������?����Ժ�

|.      |�����˰�§����?������?����              |+      |??�ʨ����§�ʨ�
|*      |��?ʨ����§�ʨ�                      |?      |��?ʨ����??�ʨ�
|{n}    |��?��?��� n ʨ�                     |{n,m}  |����?� n ʨ�Ժ����§� m ʨ�
|~|     |�ư������Ժ�{{a~|b}} ��?��� a ��� b    | -     |����?����Ժ���?����?������?��
|^      |??�˰����˵?���??��?�                  |$      |˰��?�
|[[...] |������???���??�???�?����                |[[^...] |������?����������
|\b     |���������������                      |\B     |������������������
|\d     |��?�?� [[0-9]                     |\D     |�����?�?� [[^0-9]
|\s     |���???��?��?�?����                    |\S     |���???�����?��?�?����
|\w     |[[A-Za-z0-9_]                   |\W     |[[^A-Za-z0-9_]
|(...)  |������?���?�??? $1..$9               |\.     |��?����?��������?�??�

?�������?����?�����?�???��� $1..$9 ??�???��??����?���?���?��Ƶ???����????�����?���������º���?��
----
��??��Ժ�\\

��?��� __/abc|def/__ ?����?��������� 'abc' ��� 'def'\\
��?��� __/bwiki/b__ ?����?��� 'wiki' ??�??���?��� 'jspwiki'?��\\
��?��� __^[[IVXMDCL]+\.__ ?����?���?���������������?�ȩ���?�?�?�?�������?���?��������?��? '.'\\
��?��� __/(-?\d+)(\d{3})/__ �??�� __$1,$2__ �����?�����§������?�?�???�����?�����? ','?��

���???��� [EditFindAndReplaceHelp] Ȱ����?��
[���§���???��?����˰����º����?�����|http://www.regular-expressions.info/javascript.html]
?�?��� [RegExp �?����|http://www.regular-expressions.info/javascriptexample.html]\\
