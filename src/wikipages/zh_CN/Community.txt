[{TableOfContents}]

!! ���?�?���˰�
������??? wiki ??�§����???��??��Ʈ��?��� JSPWiki ���?�?���˰�?������������Ʈ��?��?¶�˰???�?�����Ȣ�?��º�����?��?�������Ȣ�?��

���������??�???���?�?���˰�Ժ�''jspwiki-users'' ��� ''jspwiki-dev''?��

!JSPWiki-user

jspwiki-user ���?�?���˰��� Apache Incubator ����ư?�����?�?���������������?�?��??�???���?�����?��?��?��?Ժ�[jspwiki-user-subscribe@incubator.apache.org|mailto:jspwiki-user-subscribe@incubator.apache.org]?�����?�?���˰��?�ʰ�??�??�  [http://mail-archives.apache.org/mod_mbox/incubator-jspwiki-user/|http://mail-archives.apache.org/mod_mbox/incubator-jspwiki-user/]

[?�?���������˰��?�ʰ�|http://www.ecyrd.com/pipermail/jspwiki-users/] ������?����§�Ժ���� Nabble ??�������??�??? [����?�ʰ�|http://www.nabble.com/JspWiki---User-f2680.html]?��

!JSPWiki-dev

���???���˰�??�˶������? JSPWiki º�������?��¶����������ʪ�§�ȣ���������?������Ժ�����??����������?��??��jspwiki-dev ���˰��� Apache ����ưԺ����?�?�Ǯ���?�?���������??���������?������???���?�?��?��?��?Ժ�[jspwiki-dev-subscribe@incubator.apache.org|mailto:jspwiki-dev-subscribe@incubator.apache.org]?�����?�?���˰��?�ʰ�??�??� [http://mail-archives.apache.org/mod_mbox/incubator-jspwiki-dev/|http://mail-archives.apache.org/mod_mbox/incubator-jspwiki-dev/]

!JSPWiki-commits

¶�����Ƣ���??����???���˰�Ժ�????�������� SVN �?����??�??���?������?����?���������?�?��??����?��?���?�?�����??��
jspwiki-commits ���˰��� Apache ����ưԺ��Ǯ���?�?���������??���������?������������?�?��?��?Ժ�[jspwiki-dev-subscribe@incubator.apache.org|mailto:jspwiki-dev-subscribe@incubator.apache.org]?�����?�?���˰��?�ʰ�??�??� http://mail-archives.apache.org/mod_mbox/incubator-jspwiki-commits/

!����?��Ƣ���

����?��Ƣ�������??��?���Ժ��Ǯ��?������Ƣ������˰���???���������?�?��?������ jspwiki-<user|dev|commits>-unsubscribe@incubator.apache.org ���������?�?��?���������?��

¶�����Ǯ��?����???�??�����?�������Ȣ�������Ժ�?�?������ JSPWiki-devԺ���? 2007�?�10���Ժ��?�ʰ�[������?�?��|http://www.ecyrd.com/pipermail/jspwiki-dev/]?��

!! ��??��˵��?�

! IrcChannel - ??�º�����������?����§�Ժ�

��� [Freenode|http://www.freenode.net] ??����??? JSPWiki [IRC|http://www.mirc.com/irc.html] Ȣ����Ժ������????Ժ�#jspwiki. ¶�������?��??�������Ժ���������?�����?������Ժ����?�?�����������?��

���˶��?����������Ժ����??���?�Ƶ���???Ȣ����������?����?�?�������Ժ�¶�������??��?�����?�������§�������Ժ�±�??��?��??Ժ�??�˶��?��?�?���?�����?��?�??�?�����������?Ķ?Ķ

Ȣ����??�����??�ƢԺ�
* [Janne Jalkanen|http://www.jspwiki.org/wiki/JanneJalkanen] (Ecyrd)Ժ���?��???? GMT+2.

! [FaceBook | http://www.facebook.com/group.php?gid=11138025370]

��§�Ժ���������� Facebook ??�ª???�??�??? JSPWiki ����?���??�???®�??�?��¶�����Ǯ��� Facebook ??�����??�ƢԺ����?�?��������Ժ�

Ժ��?���?Ժ����?��??�?�������???�����?��??���?�����������??��??����Ժ����??????������?��?�����???���?��??�??���� Facebook ??�Ժ����?��??������������???�???Ȣ����??�??����Ժ�
-- Janne 2007�?�9���17��?

* [JSPWiki º��������Ƣ|http://blog.jspwiki.org/]
