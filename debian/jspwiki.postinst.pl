#!/usr/bin/perl

use Debconf::Client::ConfModule ':all';

my $application = get("jspwiki/applicationname");
my $pageprovider = get("jspwiki/pageprovider");
my $usepagecache = get("jspwiki/usepagecache");
my $attachmentprovider = get("jspwiki/attachments/provider");
my $baseurl = get("jspwiki/baseurl");
my $encoding = get("jspwiki/encoding");
my $breaktitles = get("jspwiki/breaktitlewithspaces");
my $matchenglishplurals = get("jspwiki/matchenglishplurals");
my $usecamelcase = get("jspwiki/camelcaselinks");
my $rssgenerate = get("jspwiki/rss/generate");
my $rssrefresh = get("jspwiki/rss/refresh");
my $rssdescription = get("jspwiki/rss/channeldescription");
my $rsslanguage = get("jspwiki/rss/channellanguage");

open(LINE, "/tmp/jspwiki.properties");
while(<LINE>) {
  s/^jspwiki\.applicationName =.*/jspwiki.applicationName = $application/;
  s/^jspwiki\.pageProvider =.*/jspwiki.pageProvider = $pageprovider/;
  s/^jspwiki\.usePageCache =.*/jspwiki.usePageCache = $usepagecache/;
  s/^jspwiki\.attachmentProvider =.*/jspwiki.attachmentProvider = $attachmentprovider/;
  s/^#jspwiki\.baseURL =.*/jspwiki.baseURL = $baseurl/;
  s/^jspwiki\.baseURL =.*/jspwiki.baseURL = $baseurl/;
  s/^jspwiki\.encoding =.*/jspwiki.encoding = $encoding/;
  s/^jspwiki\.breakTitleWithSpaces =.*/jspwiki.breakTitleWithSpaces = $breaktitles/;
  s/^jspwiki\.translatorReader\.matchEnglishPlurals =.*/jspwiki.translatorReader.matchEnglishPlurals = $matchenglishplurals/;
  s/^jspwiki\.translatorReader\.camelCaseLinks =.*/jspwiki.translatorReader.camelCaseLinks = $usecamelcase/;
  s/^jspwiki\.rss.generate =.*/jspwiki.rss.generate = $rssgenerate/;
  s/^jspwiki\.rss.interval =.*/jspwiki.rss.interval = $rssinterval/;
  s/^jspwiki\.rss.channelDescription =.*/jspwiki.rss.channelDescription = $rssdescription/;
  s/^jspwiki\.rss.channelLanguage =.*/jspwiki.rss.channelLanguage = $rsslanguage/;
}
