#!/bin/sh -e

VERSION=$2
TAR=../jspwiki_$VERSION.orig.tar.xz
DIR=jspwiki-$VERSION
TAG=$(echo jspwiki_$VERSION | sed -e 's/[\.~]/_/g')

svn export http://svn.apache.org/repos/asf/jspwiki/tags/$TAG $DIR
tar -c -J -f $TAR --exclude '*.jar' --exclude '.settings' --exclude '.classpath' --exclude '.project' $DIR
rm -rf $DIR ../$TAG $3
