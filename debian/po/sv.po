# Translation of debconf teamplates for jspwiki to Swedish
# Copyright (C) 2008-2009 Martin Bagge <brother@bsnet.se>
# This file is distributed under the same license as the jspwiki package.
# 
# Martin Bagge <brother@bsnet.se>, 2008, 2009.
msgid ""
msgstr ""
"Project-Id-Version: jspwiki 2.2.20-1\n"
"Report-Msgid-Bugs-To: jspwiki@packages.debian.org\n"
"POT-Creation-Date: 2009-09-13 19:02+0200\n"
"PO-Revision-Date: 2009-01-08 14:09+0100\n"
"Last-Translator: Martin Bagge <brother@bsnet.se>\n"
"Language-Team: Swedish <debian-l10n-swedish@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#. Type: string
#. Description
#: ../jspwiki.templates:2001 ../jspwiki.templates:3001
msgid "Default application name:"
msgstr "Applikationsnamn:"

#. Type: string
#. Description
#: ../jspwiki.templates:2001
msgid ""
"Please enter the name of the wiki. This name appears in HTML titles and log "
"files, and is usually the same as the top level URL (for instance 'http://"
"www.example.org/JSPWiki')."
msgstr ""
"Ange namnet på wikin. Namnet visas i titel-taggen och loggningen. Det är "
"vanligen samma som webnamnet (http://www.exempel.se/JSPWiki) men kan också "
"vara något helt annat."

#. Type: string
#. Description
#: ../jspwiki.templates:3001
msgid ""
"Please enter the HTTP prefix of the wiki. This rewrites all JSPWiki internal "
"link references, so it needs to be correct. It also needs to contain the "
"trailing slash."
msgstr ""
"Ange HTTP-prefixet för wikin. Detta kommer att ändra alla interna JSPWiki-"
"länkar och måste därför vara helt korrekt. Glöm inte bort det avslutande "
"snedstrecket."

#. Type: select
#. Description
#: ../jspwiki.templates:4001
msgid "Page storage mechanism to be used by JSPWiki:"
msgstr "Lagringsmetod för sidor i JSPWiki:"

#. Type: select
#. Description
#: ../jspwiki.templates:4001
msgid "Please choose the mechanism that should be used for storing pages:"
msgstr "Ange vilken metod som ska användas för att lagra sidor:"

#. Type: select
#. Description
#: ../jspwiki.templates:4001
msgid ""
" FileSystemProvider:     simply storing pages as files;\n"
" RCSFileProvider:        using an external Revision Control System;\n"
" VersioningFileProvider: versioning storage implemented in Java."
msgstr ""
" FileSystemProvider:     sparar sidorna som enskilda filer på disk;\n"
" RCSFileProvider:        externt versionshanteringssystem;\n"
" VersioningFileProvider: versionshanteringssystem skrivet i Java."

#. Type: boolean
#. Description
#: ../jspwiki.templates:5001
msgid "Should JSPWiki use a page cache?"
msgstr "Ska JSPWiki använda sidcachning?"

#. Type: boolean
#. Description
#: ../jspwiki.templates:5001
msgid "Page caching usually improves performance but increases memory usage."
msgstr ""
"Detta brukar normalt sett öka prestandan men samtidigt ökas "
"minnesanvändningen."

#. Type: string
#. Description
#: ../jspwiki.templates:6001
msgid "JSPWiki base URL:"
msgstr "Utgångs-URL för JSPWiki:"

#. Type: string
#. Description
#: ../jspwiki.templates:6001
msgid "Base URLs are used to rewrite all of JSPWiki's internal references."
msgstr ""
"Utgångs-URL används för att skriva om alla interna referenser i JSPWiki."

#. Type: string
#. Description
#: ../jspwiki.templates:6001
msgid "A trailing slash ('/') character is mandatory."
msgstr "URL:en måste avslutas med ett snedstreck (\"/\")"

#. Type: select
#. Description
#: ../jspwiki.templates:7001
msgid "Page encoding:"
msgstr "Sidkodning"

#. Type: select
#. Description
#: ../jspwiki.templates:7001
msgid ""
"Please choose which character encoding should be used by JSPWiki. UTF-8 is "
"strongly recommended."
msgstr ""
"Ange vilken teckenkodning som skall användas av JSPWiki. UTF-8 rekomenderas."

#. Type: boolean
#. Description
#: ../jspwiki.templates:8001
msgid "Break at capitals in page names?"
msgstr "Bryta vid versaler i sidnamn?"

#. Type: boolean
#. Description
#: ../jspwiki.templates:8001
msgid ""
"Please choose whether page titles should be rendered using an extra space "
"after each capital letter. This causes 'RSSPage' to be shown as 'R S S Page'."
msgstr ""
"Ska sidtitlar ritas upp med ett extra mellanslag efter varje versal. Detta "
"innebär att \"RSSPage\" visas som \"R S S Page\"."

#. Type: boolean
#. Description
#: ../jspwiki.templates:9001
msgid "Match plural form to singular form in page names?"
msgstr "Matcha pluralform till singularform i sidnamn?"

#. Type: boolean
#. Description
#: ../jspwiki.templates:9001
msgid ""
"Choosing this option will cause JSPWiki to match 'PageLinks' to 'PageLink' "
"if 'PageLinks' is not found."
msgstr ""
"Väljs detta kommer JSPWiki att peka  \"PageLinks\" till \"PageLink\" om "
"\"PageLinks\" inte hittas."

#. Type: boolean
#. Description
#: ../jspwiki.templates:10001
msgid "Use CamelCase links?"
msgstr "Använda CamelCase-länkar?"

#. Type: boolean
#. Description
#: ../jspwiki.templates:10001
msgid ""
"Please choose whether JSPWiki should consider traditional WikiNames (names "
"of pages JustSmashedTogether without square brackets) as hyperlinks."
msgstr ""
"Ange om JSPWiki ska betrakta traditionella WikiNamn (namn på sidor "
"SomTrycksIhop utan hakparenteser) som hyperlänkar."

#. Type: boolean
#. Description
#: ../jspwiki.templates:11001
msgid "Generate RSS feed?"
msgstr "Skapa RSS-ström?"

#. Type: boolean
#. Description
#: ../jspwiki.templates:11001
msgid ""
"JSPWiki can generate a Rich Site Summary feed so that users can track "
"changes to the wiki."
msgstr ""
"JSPWiki kan skapa en Rich Site Summary-ström. Detta gör att användare "
"enklare kan se om något har ändrats på wikin."

#. Type: string
#. Description
#: ../jspwiki.templates:12001
msgid "RSS refresh time in seconds:"
msgstr "RSS, uppdateringstid i sekunder"

#. Type: string
#. Description
#: ../jspwiki.templates:12001
msgid "Please choose the delay between RSS feed refreshes."
msgstr "Ange intervall mellan uppdateringar av RSS-strömmar"

#. Type: string
#. Description
#: ../jspwiki.templates:13001
msgid "RSS channel description:"
msgstr "RSS, strömbeskrivning"

#. Type: string
#. Description
#: ../jspwiki.templates:13001
msgid ""
"Please give a channel description for the RSS feed, to be shown in channel "
"catalogs. There is no maximum length, so include whatever details users may "
"find helpful."
msgstr ""
"Ange en beskrivning som används tillsammans med RSS-strömmen. Det finns "
"ingen maxlängd på texten, var så utförlig som möjligt."

#. Type: string
#. Description
#: ../jspwiki.templates:14001
msgid "RSS channel language:"
msgstr "RSS, strömspråk"

#. Type: string
#. Description
#: ../jspwiki.templates:14001
msgid ""
"Please choose the RSS feed language. This should match the language of the "
"wiki."
msgstr "Ange språket för RSS-strömmen. Det bör vara samma som språket i wikin."

#. Type: select
#. Choices
#: ../jspwiki.templates:15001
msgid "nothing"
msgstr "inget"

#. Type: select
#. Description
#: ../jspwiki.templates:15002
msgid "Attachment storage mechanism to use:"
msgstr "Lagringsmetod för bifogade filer:"

#. Type: select
#. Description
#: ../jspwiki.templates:15002
msgid ""
"'BasicAttachmentProvider' uses the same directory structure as the selected "
"page storage mechanism. It simply stores the attachments in a dedicated "
"directory for a page."
msgstr ""
"\"BasicAttachmentProvider\" använder samma mappstruktur som den valda "
"sidleverantören. Den sparar helt enkelt bifogningar i en dedikerad mapp för "
"en sida."

#. Type: boolean
#. Description
#: ../jspwiki.templates:16001
msgid "Should all wiki pages be deleted on package purge?"
msgstr "Ska ALLA wiki-sidor raderas när paketet tas bort?"

#. Type: boolean
#. Description
#: ../jspwiki.templates:16001
msgid ""
"Please choose whether you want all wiki pages to be removed when the package "
"is purged."
msgstr "Ange om du vill att alla wikisidor ska tas bort när paketet tas bort."
