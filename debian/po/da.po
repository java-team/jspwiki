# Danish translation jspwiki.
# Copyright (C) 2010 jspwiki & Joe Hansen.
# This file is distributed under the same license as the jspwiki package.
# Joe Hansen <joedalton2@yahoo.dk>, 2010. 
#
msgid ""
msgstr ""
"Project-Id-Version: jspwiki\n"
"Report-Msgid-Bugs-To: jspwiki@packages.debian.org\n"
"POT-Creation-Date: 2009-09-13 19:02+0200\n"
"PO-Revision-Date: 2010-05-05 17:30+01:00\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org> \n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#. Type: string
#. Description
#: ../jspwiki.templates:2001 ../jspwiki.templates:3001
msgid "Default application name:"
msgstr "Standardprogramnavn:"

#. Type: string
#. Description
#: ../jspwiki.templates:2001
msgid ""
"Please enter the name of the wiki. This name appears in HTML titles and log "
"files, and is usually the same as the top level URL (for instance 'http://"
"www.example.org/JSPWiki')."
msgstr ""
"Indtast venligst navnet på wikien. Dette navn fremstår i HTML-titler og "
"logfiler, og er normalt det samme som URL'en på øverste niveau (for eksempel "
"http://www.example.org/JSPWiki)."

#. Type: string
#. Description
#: ../jspwiki.templates:3001
msgid ""
"Please enter the HTTP prefix of the wiki. This rewrites all JSPWiki internal "
"link references, so it needs to be correct. It also needs to contain the "
"trailing slash."
msgstr ""
"Indtast venligst HTTP-præfikset på wikien. Dette genskriver alle interne "
"henvisningsreferencer til JSPWiki, så det skal rettes. Den skal også "
"indeholde skråstreg."

#. Type: select
#. Description
#: ../jspwiki.templates:4001
msgid "Page storage mechanism to be used by JSPWiki:"
msgstr "Mekanisme for sidegemning som skal bruges af JSPWiki:"

#. Type: select
#. Description
#: ../jspwiki.templates:4001
msgid "Please choose the mechanism that should be used for storing pages:"
msgstr "Vælg venligst mekanismen som skal bruges til at gemme sider:"

#. Type: select
#. Description
#: ../jspwiki.templates:4001
msgid ""
" FileSystemProvider:     simply storing pages as files;\n"
" RCSFileProvider:        using an external Revision Control System;\n"
" VersioningFileProvider: versioning storage implemented in Java."
msgstr ""
" Filsystemudbyder:        Gem sider som filer;\n"
" RCS-filudbyder:          Brug af ekstern revisionskontrolsystem;\n"
" Versioneringsfiludbyder: Versioneringslager implementeret i Java."

#. Type: boolean
#. Description
#: ../jspwiki.templates:5001
msgid "Should JSPWiki use a page cache?"
msgstr "Skal JSPWiki bruge sidemellemlager (cache)?"

#. Type: boolean
#. Description
#: ../jspwiki.templates:5001
msgid "Page caching usually improves performance but increases memory usage."
msgstr ""
"Sidemellemlager (cache) forbedrer normalt ydelsen men øger "
"hukommelsesforbruget."

#. Type: string
#. Description
#: ../jspwiki.templates:6001
msgid "JSPWiki base URL:"
msgstr "JSPWikis base-URL:"

#. Type: string
#. Description
#: ../jspwiki.templates:6001
msgid "Base URLs are used to rewrite all of JSPWiki's internal references."
msgstr "Base-URL'er bruges til at genskrive alle JSPWikis interne referencer."

#. Type: string
#. Description
#: ../jspwiki.templates:6001
msgid "A trailing slash ('/') character is mandatory."
msgstr "En skråstreg (»/«) er obligatorisk."

#. Type: select
#. Description
#: ../jspwiki.templates:7001
msgid "Page encoding:"
msgstr "Sidekodning:"

#. Type: select
#. Description
#: ../jspwiki.templates:7001
msgid ""
"Please choose which character encoding should be used by JSPWiki. UTF-8 is "
"strongly recommended."
msgstr ""
"Vælg venligst hvilken tegnkodning der skal bruges af JSPWiki. UTF-8 "
"anbefales stærkt."

#. Type: boolean
#. Description
#: ../jspwiki.templates:8001
msgid "Break at capitals in page names?"
msgstr "Ekstra plads ved store bogstaver i sidenavne?"

#. Type: boolean
#. Description
#: ../jspwiki.templates:8001
msgid ""
"Please choose whether page titles should be rendered using an extra space "
"after each capital letter. This causes 'RSSPage' to be shown as 'R S S Page'."
msgstr ""
"Vælg venligst om sidetitler skal optegnes med brug af ekstra plads efter "
"hvert stort bogstav. Dette betyder at »RSSPage« bliver vist som »R S S Page«."

#. Type: boolean
#. Description
#: ../jspwiki.templates:9001
msgid "Match plural form to singular form in page names?"
msgstr "Match flertalsform med entalsform i sidenavne?"

#. Type: boolean
#. Description
#: ../jspwiki.templates:9001
msgid ""
"Choosing this option will cause JSPWiki to match 'PageLinks' to 'PageLink' "
"if 'PageLinks' is not found."
msgstr ""
"Valg af denne indstilling vil få JSPWiki til at matche »PageLinks« med "
"»PageLink« hvis »PageLinks« ikke findes."

#. Type: boolean
#. Description
#: ../jspwiki.templates:10001
msgid "Use CamelCase links?"
msgstr "Brug sammensatte henvisninger (CamelCase)?"

#. Type: boolean
#. Description
#: ../jspwiki.templates:10001
msgid ""
"Please choose whether JSPWiki should consider traditional WikiNames (names "
"of pages JustSmashedTogether without square brackets) as hyperlinks."
msgstr ""
"Vælg venligst hvorvidt JSPWiki skal anse traditionelle Wikinavne (navne på "
"sider JustSmashedTogether uden klammer) som hyperhenvisninger."

#. Type: boolean
#. Description
#: ../jspwiki.templates:11001
msgid "Generate RSS feed?"
msgstr "Opret RSS-nyhedskilde?"

#. Type: boolean
#. Description
#: ../jspwiki.templates:11001
msgid ""
"JSPWiki can generate a Rich Site Summary feed so that users can track "
"changes to the wiki."
msgstr ""
"JSPWiki kan oprette en nyhedskilde af typen Rich Site Summary så brugere kan "
"spore ændringer på wikien."

#. Type: string
#. Description
#: ../jspwiki.templates:12001
msgid "RSS refresh time in seconds:"
msgstr "RSS-opdateringstid i sekunder:"

#. Type: string
#. Description
#: ../jspwiki.templates:12001
msgid "Please choose the delay between RSS feed refreshes."
msgstr "Vælg venligst forsinkelsen på opdateringer mellem RSS-nyhedskilder"

#. Type: string
#. Description
#: ../jspwiki.templates:13001
msgid "RSS channel description:"
msgstr "RSS-kanalbeskrivelse:"

#. Type: string
#. Description
#: ../jspwiki.templates:13001
msgid ""
"Please give a channel description for the RSS feed, to be shown in channel "
"catalogs. There is no maximum length, so include whatever details users may "
"find helpful."
msgstr ""
"Giv venligst en kanalbeskrivelse på RSS-nyhedskilden, som vises i "
"kanalkataloger. Der er ingen maksimal længde, så inkluder alle de detaljer "
"som kan hjælpe brugerne."

#. Type: string
#. Description
#: ../jspwiki.templates:14001
msgid "RSS channel language:"
msgstr "RSS-kanalsprog:"

#. Type: string
#. Description
#: ../jspwiki.templates:14001
msgid ""
"Please choose the RSS feed language. This should match the language of the "
"wiki."
msgstr ""
"Vælg venligst RSS-nyhedskildesproget. Dette skal svare til sproget på wikien."

#. Type: select
#. Choices
#: ../jspwiki.templates:15001
msgid "nothing"
msgstr "intet"

#. Type: select
#. Description
#: ../jspwiki.templates:15002
msgid "Attachment storage mechanism to use:"
msgstr "Lagermekanisme til bilag der skal bruges:"

#. Type: select
#. Description
#: ../jspwiki.templates:15002
msgid ""
"'BasicAttachmentProvider' uses the same directory structure as the selected "
"page storage mechanism. It simply stores the attachments in a dedicated "
"directory for a page."
msgstr ""
"»BasicAttachmentProvider« bruger den samme mappestruktur som den valgte "
"sidelagermekanisme. Den gemmer bilagene i en dedikeret mappe for en side."

#. Type: boolean
#. Description
#: ../jspwiki.templates:16001
msgid "Should all wiki pages be deleted on package purge?"
msgstr "Skal alle wikisider slettes ved pakkeoprydning?"

#. Type: boolean
#. Description
#: ../jspwiki.templates:16001
msgid ""
"Please choose whether you want all wiki pages to be removed when the package "
"is purged."
msgstr ""
"Vælg venligst hvorvidt du ønsker, at alle wikisider skal fjernes, når pakken "
"ryddes op."
